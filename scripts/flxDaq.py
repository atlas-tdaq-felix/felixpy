from time import sleep
import libFlxPy as flx

print "WOOF -> Create a FlxDaq..."
memSize=1024*1024*1024
cardNo=0
dmaIdx=0
daq=flx.FlxDaq(cardNo, memSize, dmaIdx)

if daq.hasError():
  print "WOOF -> Something went wrong: " + str(daq.errorString())
  exit()
else:
  print "WOOF -> Your FlxDaq is good to go!"

print "WOOF -> Some details... "
print "  -> fanOutLocked? - " + str(daq.fanOutLocked())
print "  -> bufferSize? - " + str(daq.bufferSize())
print "  -> firmwareVersionString? - " + str(daq.firmwareVersionString())
print "  -> numberOfChanels? - " + str(daq.numberOfChans())

print '\n'

print "WOOF -> Starting emulator..."
externalEmu=False
daq.startEmulator(externalEmu)

print "WOOF -> Start recording..."
fileName='flxData.dat'
runNumber=0
perElink=False
daq.startRecording(fileName, runNumber, perElink)

print "\n zzz 100 msec \n"
sleep(0.1)

print "WOOF -> Stop recording, and stop emulator..."
daq.stopRecording()
daq.stopEmulator()

print '\n'

print "WOOF -> Dump statistics..."
print "  -> bytesReceivedCount? - " + str(float(daq.bytesReceivedCount())/1024/1024) + " MByte"
print "  -> bytesWrittenCount?  - " + str(float(daq.bytesWrittenCount())/1024/1024) + " MByte"
print "  -> bytesRecordedCount? - " + str(float(daq.bytesRecordedCount())/1024/1024) + " MByte"

print "WOOF -> Clean the house... (daq stop and delete)"
daq.stop()
del daq

