# Same for fdaq.
cmake_minimum_required(VERSION 3.5)

cmake_policy(SET CMP0074 NEW)

set(PACKAGE felixpy)
set(PACKAGE_VERSION 0.0.0)

set(LIBOPTS_NOINSTALL)
set(BINOPTS_NOINSTALL)


include(FELIX)

# Compiler setup.
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    message(FATAL_ERROR "GCC version must be at least 4.8!")
  endif()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -qopt-prefetch -unroll-aggressive -march=native -mtune=native")
endif()

## Dependencies
include_directories(${CMAKE_BINARY_DIR})

felix_add_external(libfabric ${libfabric_version} ${BINARY_TAG})
felix_add_external(pybind11 ${pybind11_version})
felix_add_external(json ${json_version})
felix_add_external(yaml-cpp ${yaml-cpp_version} ${BINARY_TAG})
felix_add_external(catch ${catch_version} ${BINARY_TAG})

# externals
felix_add_external(felix-drivers ${felix_drivers_version} ${BINARY_TAG})

find_package(TBB ${tbb_version} EXACT REQUIRED)
include_directories(${TBB_INCLUDE_DIRS})
link_directories(${TBB_DIR}/lib64)

# Qt stuff (For FlxDaq moc-s)
set(CMAKE_AUTOMOC OFF)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# No FindQT5.cmake yet
#find_package(Qt5 ${qt5_version} EXACT REQUIRED)
include_directories(${QT5_DIR}/include)
include_directories(${QT5_DIR}/include/QtCore)
link_directories(${QT5_DIR}/lib)

#python
#find_package(PythonLibs ${pythonlibs_version} EXACT REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})

#pybind11
include_directories(pybind11/include)

# Sources of this project
include_directories(src)

# FlxPy
add_definitions(-std=c++17 -Wall -pedantic)
file(GLOB FLXPY_SOURCES src/FlxPy.cpp ../ftools/src/*.cpp)
include_directories(../ftools/lpgbt-regmap)
tdaq_add_library(FlxPy NOINSTALL ${FLXPY_SOURCES})
#add_dependencies(FlxPy # EXTRA DEPEND FILES IF NEEDED)
set_target_properties(FlxPy PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(FlxPy FlxTools Qt5Core netio tbb ${PYTHON_LIBRARY})

## Install
#install(TARGETS FlxPy DESTINATION lib)
#install(PROGRAMS scripts/flxDaq.py DESTINATION bin)
