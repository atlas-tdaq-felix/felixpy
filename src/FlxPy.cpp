// WARNING: This piece of code is heavily under development!
// // Please don't change it.
// // In the case of any misbehaviour, contact me.
// //
// // author: Roland Sipos
// // mail: roland.sipos@cern.ch

#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>

#include "../ftools/src/FlxDaq.h"
#include "../ftools/src/FlxUpload.h"
//#include "ftools/FlxDaq.h"
//#include "ftools/FlxUpload.h"

#include "NetioWorker.hh"

#undef _POSIX_C_SOURCE
#undef _XOPEN_SOURCE

#include "pybind11/pybind11.h"
namespace py = pybind11;

class FlxDaq;
class FlxUpload;

PYBIND11_MODULE(libFlxPy, m) {
  m.doc() = "Python bindings for several FELIX utilities and classes.";

  // Exposing FlxDaq towards Python.
  py::class_<FlxDaq>(m, "FlxDaq")
    .def(py::init<int, int, int>())
    .def("classVersion", &FlxDaq::classVersion)
    .def("errorString",  &FlxDaq::errorString)
    .def("hasError",     &FlxDaq::hasError)
    .def("debugString",  &FlxDaq::debugString)
    .def("stop", &FlxDaq::stop)
    .def("setUseInterrupt", &FlxDaq::setUseInterrupt)

    .def("enableBackpressure", &FlxDaq::enableBackpressure)
    .def("setFileNameDateTime", &FlxDaq::setFileNameDateTime)
    .def("fanOutLocked", &FlxDaq::fanOutLocked)
    .def("setFanOutForDaq", &FlxDaq::setFanOutForDaq)

    .def("startRecording",         &FlxDaq::startRecording)
    .def("stopRecording",          &FlxDaq::stopRecording)
    .def("startEmulator",          &FlxDaq::startEmulator)
    .def("stopEmulator",           &FlxDaq::stopEmulator)
    .def("fileName",               &FlxDaq::fileName)
    .def("fileMaxSize",            &FlxDaq::fileMaxSize)
    .def("setFileMaxSize",         &FlxDaq::setFileMaxSize)
    .def("setFlush",               &FlxDaq::setFlush)
    .def("bufferSize",             &FlxDaq::bufferSize)
    .def("setBufferSize",          &FlxDaq::setBufferSize)
    .def("bufferEmpty",            &FlxDaq::bufferEmpty)
    .def("bufferFull",             &FlxDaq::bufferFull)
    .def("bufferFullOccurred",     &FlxDaq::bufferFullOccurred)
    .def("resetBufferFullOccured", &FlxDaq::resetBufferFullOccurred)
    .def("dataBuffer",             &FlxDaq::dataBuffer)
    //.def("dataBufferPy", [](){ return py::bytes(&FlxDaq::dataBuffer); }
    .def("dataBufferPy", [](FlxDaq& self) { return py::bytes(self.dataBuffer()); }, py::return_value_policy::reference)

    .def("firmwareVersionString",  &FlxDaq::firmwareVersionString)
    .def("numberOfChans",          &FlxDaq::numberOfChans)
    .def("bytesReceivedCount", &FlxDaq::bytesReceivedCount)
    .def("bytesWrittenCount",  &FlxDaq::bytesWrittenCount)
    .def("bytesRecordedCount", &FlxDaq::bytesRecordedCount)
    .def("bytesRecordedInRunCount", &FlxDaq::bytesRecordedInRunCount)
    .def("bytesFlushedCount", &FlxDaq::bytesFlushedCount)
    .def("bufferWrapCount", &FlxDaq::bufferWrapCount)
    .def("bufferFullPercentage", &FlxDaq::bufferFullPercentage)
    .def("fileCount", &FlxDaq::fileCount);


  // Exposing NetioWorker towards Python.
  py::class_<NetioWorker>(m, "NetioWorker")
    .def(py::init<const unsigned&, const std::string&, const unsigned&>())
    .def("queueIn", &NetioWorker::queueIn)
    .def("queueOut", &NetioWorker::queueOut);

}

