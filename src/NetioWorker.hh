#ifndef NETIO_WORKER_HH_
#define NETIO_WORKER_HH_

#include "ProducerConsumerQueue.h"

#include "netio/netio.hpp"

#include <thread>
#include <iostream>
#include <vector>
#include <bitset>

class NetioWorker {

public:

  /* Explicitly using the default constructor to
  *  underline the fact that it does get called. */
  NetioWorker(const unsigned& tag, const std::string& host, const unsigned& port) 
    : worker_thread(), sub_socket( new netio::context(std::string("posix"))), pcq(1000000)
  { 
    sub_socket.subscribe(tag, netio::endpoint(host, port));
  }

  ~NetioWorker() {
    stop_thread = true;
    if (worker_thread.joinable())
      worker_thread.join();
  }

  // Actually start the thread. Notice Move semantics!
  void queueIn() {
    worker_thread = std::thread(&NetioWorker::QueueMessages, this);
  }

  void queueOut(){ //char* buffer, size_t* bytes_read ) {
    worker_thread = std::thread(&NetioWorker::DequeueMessages, this); //, buffer, bytes_read);
    worker_thread.join();
  }

  void join() {
    stop_thread = true;
    if (worker_thread.joinable())
      worker_thread.join();
  }

private:
  // Actual thread handler members.
  std::thread worker_thread;
  std::atomic_bool stop_thread{ false };

  // References coming from the HWInterface.
  netio::subscribe_socket sub_socket;
  folly::ProducerConsumerQueue<netio::message> pcq;

  // The main working thread for queueing in received messages.
  void QueueMessages() {
    //unsigned long long bytes_received = 0;
    while(!stop_thread) {
      // subscribe to netio socket and read messages constantly.
      netio::message m;
      sub_socket.recv(m);
      std::cout << "WOOF -> Received message! \n";

      // Handle message or just print out...
      //bytes_received += m.size();
      //pcq.enqueue( std::move(m) ); //bool done = pcq.enqueue(m);
    }
  }

  // The main worker thread for dequeuing messages.
  void DequeueMessages(){
    std::cout << "WOOF -> I should deque a single message.\n";
    netio::message msgObj;
    pcq.try_dequeue( std::ref(msgObj) );
  }

};

#endif /* NETIO_WORKER_HH_ */

